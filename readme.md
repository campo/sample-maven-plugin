# Sample maven plugin

[How to Build a Maven Plugin | Okta Developer](https://developer.okta.com/blog/2019/09/23/tutorial-build-a-maven-plugin)

[Maven – Guide to Developing Java Plugins](http://maven.apache.org/guides/plugin/guide-java-plugin-development.html)

[Maven – POM Reference - The Super POM](http://maven.apache.org/pom.html#The_Super_POM)

[How to write your own Maven plugins](https://blogs.oracle.com/javamagazine/how-to-write-your-own-maven-plugins)