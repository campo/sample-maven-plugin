import org.redundent.kotlin.xml.xml

data class XmlDocument(val name: String, val version: String, val title: String = name)

fun generateDescriptor(documentList: Iterable<XmlDocument>): String {
    return xml("xmlDox") {
        for (xmlDocument in documentList) {
            "xmlDocument" {
                attribute("name", xmlDocument.name)
                attribute("version", xmlDocument.version)
                attribute("title", xmlDocument.title)
            }
        }
    }.toString()
}