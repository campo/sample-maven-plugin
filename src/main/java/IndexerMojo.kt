import com.google.gson.Gson
import org.apache.maven.plugin.AbstractMojo
import org.apache.maven.plugin.MojoExecutionException
import org.apache.maven.plugins.annotations.LifecyclePhase
import org.apache.maven.plugins.annotations.Mojo
import org.apache.maven.plugins.annotations.Parameter
import org.apache.maven.project.MavenProject
import org.jetbrains.annotations.NotNull
import java.io.File
import java.util.*

@Mojo(name = "index", defaultPhase = LifecyclePhase.GENERATE_RESOURCES)
class IndexerMojo : AbstractMojo() {

    @Parameter(
        property = "resourceDir",
        defaultValue = "\${project.basedir}/src/main/resources"
    )
    @NotNull
    private val resourceDir: String? = null

    @Parameter(
        property = "targetDir",
        defaultValue = "\${project.build.directory}"
    )
    @NotNull
    private val targetDir: String = "."

    @NotNull
    @Parameter(property = "project", readonly = true)
    private val project: MavenProject? = null

    override fun execute() {

        if (project == null)
            throw MojoExecutionException("project can't be null")

        val version = project.version
        log.info("Project version is: $version")
        log.info("Scanning directory: $resourceDir")

        val filesSequence = File(resourceDir ?: ".").walk()
            .filter { file ->
                file.isFile && file.extension in setOf("xml", "xsl", "xsd", "md")
            }.asSequence()
        val docList = filesSequence
            .map {
                log.info("Found ${it.name} (${it.canonicalPath})")
                val result = when (it.extension) {
                    "md" -> indexMarkdown(it)
                    else -> indexXmlDocument(it)
                }
                log.info(result.toString())
                result
            }
        val descriptor = generateDescriptor(docList.asIterable())

        val directory = File(targetDir)
        if (!directory.exists()) {
            directory.mkdirs()
        }
        val xFile = File(targetDir, "catalog.xml")
        xFile.writeText(descriptor)
        log.info("${xFile.canonicalPath} written")

        val jsonFile = File(targetDir, "catalog.json")
        val gson = Gson()
        jsonFile.writeText(gson.toJson(docList.asIterable()))
        log.info("${jsonFile.canonicalPath} written")
    }

    fun indexMarkdown(file: File): XmlDocument {
        val token = """^\s*#[^#]+""".toRegex()
        val headline: Optional<String> = file.bufferedReader()
            .lines()
            .limit(10)
            .filter { token.find(it) != null }
            .map { it.replaceFirst("""# """, "") }
            .peek { println(it) }
            .findFirst()
        return XmlDocument(name = file.name, version = "1.2.3", title = headline.orElse(file.name))
    }

    fun indexXmlDocument(file: File): XmlDocument {
        return XmlDocument(name = file.name, version = "1.2.3", title = file.canonicalPath)
    }
}