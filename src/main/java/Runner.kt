import com.lordcodes.turtle.shellRun

class Runner {

    fun getVersion(): String {
        val output = shellRun("git", listOf("rev-parse", "--abbrev-ref", "HEAD"))
        return output
    }

}