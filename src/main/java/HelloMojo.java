import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.apache.maven.project.MavenProject;

import java.util.function.BiConsumer;

@Mojo(name = "hello", defaultPhase = LifecyclePhase.INITIALIZE)
public class HelloMojo extends AbstractMojo {

    /**
     * The git command used to retrieve the current commit hash.
     */
    @Parameter(property = "git.command", defaultValue = "ls -la")
    private String command;

    @Parameter(property = "project", readonly = true)
    private MavenProject project;

    @Override
    public void execute() throws MojoExecutionException, MojoFailureException {

        // call the getVersion method
        String version = new Runner().getVersion();

        // define a new property in the Maven Project
        project.getProperties().put("exampleVersion", version);

        // Maven Plugins have built in logging too
        getLog().info("Git command: " + command);
        getLog().info("Git hash: " + version);

        project.getProperties().forEach(tell);

    }

    BiConsumer<Object, Object> tell = (Object k, Object v) ->
            getLog().info(k +
                    " = " +
                    v);
}

